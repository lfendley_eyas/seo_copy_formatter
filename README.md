# README #

Webapp to quickly format SEO copy by trimming trailing -h2 text and wrapping copy in H2/p tags depending on title match.

### How do I use it? ###

* Paste in the desired copy
* Click Format
* Click 'Copy to Clipboard'
* Ready to Paste into 'Games Release'