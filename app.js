'use strict'

let pastedCopy = document.querySelector('#seo-copy');
const formatBtn = document.querySelector('#format');
const clearBtn = document.querySelector('#clear');
const headingRegex = /- H2/ig;

let formatCopytoHTML = (copy) => {
    let formatted = copy.value.replace(headingRegex, ''); // Remove trailing - H2 from copy
    let textArea = formatted.split('\n').map(e => e.trim()).filter(e => e);
    let headingTitles = ["Introduction", "Game Background", "Symbols & Features", "Bonuses", "Summary"];

    textArea.forEach((element, i) => {
        if (headingTitles.includes(element)) {
            textArea[i] = `<h2>${element}</h2>`;
        } else {
            textArea[i] = `<p>${element}</p>`;
        }
    });

    pastedCopy.value = textArea.join('\n');
};

let copyToClipboard = (elementId) => {
    let cc = document.createElement("input");

    cc.setAttribute("value", document.getElementById(elementId).value);
    document.body.appendChild(cc);
    cc.select();
    document.execCommand("copy");
    document.body.removeChild(cc);
}

formatBtn.addEventListener('click', () => {
    formatCopytoHTML(pastedCopy); // invoke function to run regex and output html formatted copy
});

clearBtn.addEventListener('click', () => {
    pastedCopy.value = ""; // empty the element
});